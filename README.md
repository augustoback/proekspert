Description:

This is a possible solution to the Binary Tree BotNet Reverter.
Each Bot is represented by an int id;
The network can have any number of Bots. The number of Bots is defined in application.properties.

The Project was built using Java 8 and Spring Boot 2.0.3.

The HTTP Port is defined in application.properties

Build:

The requirements to run the project are Maven 3.5 and JDK8+.
To execute the project, please run this command line:
mvn spring-boot:run

Command line example:
The entry point of the program is : http://localhost:8080/v1/0/reverse/

where 0 is the bot id. This Id can be any number below the number defined in application.properties.

curl --request GET \
  --url http://localhost:8080/v1/0/reverse/ \
  --header 'content-type: application/json' \
  --data '{
   "btreeId":6,
   "id":4,
   "value":1,
   "leftNode":{
      "btreeId":6,
      "id":2,
      "value":3,
      "leftNode":{
         "btreeId":6,
         "id":6,
         "value":6,
         "leftNode":null,
         "rightNode":null
      },
      "rightNode":{
         "btreeId":6,
         "id":7,
         "value":8,
         "leftNode":null,
         "rightNode":null
      }
   },
   "rightNode":{
      "btreeId":6,
      "id":3,
      "value":5,
      "leftNode":{
         "btreeId":6,
         "id":8,
         "value":9,
         "leftNode":null,
         "rightNode":null
      },
      "rightNode":{
         "btreeId":6,
         "id":9,
         "value":10,
         "leftNode":null,
         "rightNode":null
      }
   }
}'
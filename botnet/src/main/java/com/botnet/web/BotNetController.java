package com.botnet.web;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.botnet.domain.Btree;
import com.botnet.helper.BotControlHelper;
import com.botnet.helper.RevertHelper;

/**
 * 
 * @author AUGUSTODEWESBACK
 * This class is the main class of the program. It is responsible for control the requests to each bot revert a Btree.
 */
@RestController
public class BotNetController {
	
	@Value( "${bots.count}" )
	private int botsCount;
	
	@Autowired
	RevertHelper revertHelper;
	
	@Autowired
	BotControlHelper botControlHelper;
	
	Logger logger = Logger.getAnonymousLogger();
	
	@RequestMapping(value="v1/{node}/reverse/",  method= {RequestMethod.GET,RequestMethod.POST})
	public Btree bot(@PathVariable("node") int bot, @RequestBody Btree btree) {
		if(bot > botsCount) {
			throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Bot not found");
		}
		Btree returnObject = null;
		if(botControlHelper.canThisBotReverseThisBtree(bot, btree)) {
			logger.log(Level.INFO, () -> "Hello, I`m bot: " + bot + " and I am reverting BtreeId: " + btree.getBtreeId() + " and node id: " + btree.getId());
			returnObject = revertHelper.revert(bot, btree);
		}else {
			int nodeTORevert = botControlHelper.findAvailableBotToReverseThisBtree(btree);
			if( nodeTORevert == -1) {
				logger.log(Level.INFO, () ->"There is no other Bot available. I`m bot: " + bot + " and I am reverting BtreeId: " + btree.getBtreeId() + " and node id: " + btree.getId());
				returnObject = revertHelper.revert(bot, btree);	
			}else {
				logger.log(Level.INFO, () ->"Ops, I`m bot: " + bot + " and I am sending BtreeId: " + btree.getBtreeId() + " and node id: " + btree.getId() + " to bot: " + nodeTORevert);
				returnObject = revertHelper.callRemoteRevert(btree, nodeTORevert);
			}
		}

		return returnObject;
	}

}

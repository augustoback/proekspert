package com.botnet.configurator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.botnet"})
public class BotnetApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BotnetApplication.class, args);
	}
}

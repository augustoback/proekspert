package com.botnet.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.botnet.domain.Btree;

/**
 * 
 * @author AUGUSTODEWESBACK
 * This class is responsible for revert a Btree and send the left and right node to be reverted by another bot if available.
 * I used a classic Recursion algorithm to revert it. 
 */
@Component
@Scope(value="singleton")
public class RevertHelper {
	
	@Autowired
	BotControlHelper botControlHelper;

	@Value( "${remote.address}" )
	private String remoteAddress;

	@Value( "${remote.address.port}" )
	private String remoteAddressPort;
	
	/**
	 * Method Auxiliar Method to revert a Btree.
	 * @param currentBot
	 * @param btree
	 * @return Btree reverted
	 */
	public Btree revert(int currentBot, Btree btree) {
		revertAux(currentBot, btree);
		return btree;
	}
	
	/**
	 * Recursion Algorithm to revert a Btree.
	 * At the begining of the method, it call the "control center" to inform that this Bot has already reverted a Btree with this ID. (Optional / Extra Point)
	 * 
	 * @param currentBot
	 * @param btree
	 */
	private void revertAux(int currentBot,Btree btree) {
		if(btree == null) {
			return;
		}
		
		botControlHelper.updateBotBtreeReference(currentBot,btree);
		
		Btree aux = btree.getLeftNode();
		
		btree.setLeftNode(btree.getRightNode());
		btree.setRightNode(aux);
		
		if(btree.getLeftNode() != null) {
			int node = getBotToRevertThisBtree(currentBot, btree.getLeftNode());
			Btree leftNode = callRemoteRevert(btree.getLeftNode(), node);
			btree.setLeftNode(leftNode);
		}
		
		if(btree.getRightNode() != null) {
			int node = getBotToRevertThisBtree(currentBot, btree.getRightNode());
			Btree rightNode = callRemoteRevert(btree.getRightNode(), node);
			btree.setRightNode(rightNode);
		}
	}

	/**
	 * This method calls the "control center" to receive the next bot to redirect a Node of the Btree.
	 * If there is no Bot Available, it returns the current bot.
	 * @param currentBot
	 * @param btree
	 * @return
	 */
	private int getBotToRevertThisBtree(int currentBot, Btree btree) {
		int node = botControlHelper.findAvailableBotToReverseThisBtree(btree);
		if(node == -1) {
			node = currentBot;
		}
		return node;
	}

	/**
	 * Method responsible is responsible for the HTTP request to another Bot
	 * @param nodeToRevert
	 * @param bot
	 * @return
	 */
	public Btree callRemoteRevert(Btree nodeToRevert, int bot) {
		String uri = remoteAddress+ remoteAddressPort + "/v1/"+bot+"/reverse/";
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.postForEntity(uri, nodeToRevert, Btree.class).getBody();
	}
	
	/**
	 * This method is used by Unit Tests to provide a way to call a remote Revert.
	 * @param port
	 */
	public void setRemoteAddressPort(String port) {
		this.remoteAddressPort = port;
	}

}

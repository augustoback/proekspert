package com.botnet.helper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.botnet.domain.Btree;

/**
 * This class is the "Control Center".
 * It is responsible for the orchestration of calls between Bots.
 * It is necessary to accomplish the Extra Point.
 * @author AUGUSTODEWESBACK
 *
 */
@Component
@Scope(value="singleton")
public class BotControlHelper {

	@Value( "${bots.count}" )
	private int botsCount;

	/**
	 * Each Bot is represented by a int. 
	 * Each Bot has a Set of which Btree it has already reverted.
	 */
	private HashMap<Integer, HashSet<Integer>> control = new HashMap<>();
			
	/**
	 * This Method is responsible for identifying if a given Bot is able to revert a given B Tree. It consults the control Map above.
	 * @param bot
	 * @param btree
	 * @return
	 */
	public synchronized boolean canThisBotReverseThisBtree(int bot, Btree btree) {
		boolean out = true;
		if(bot > botsCount) {
			out = false;
		}else if(control.get(bot) == null) {
			return out;
		}else if(control.get(bot).contains(btree.getBtreeId())) {
			out = false;
		}
		return out;
	}
	
	/**
	 * It is responsible for finding a Bot that has not already reverted this Btree.
	 * If there is no Bot available it returns -1.
	 * @param btree
	 * @return
	 */
	public synchronized int findAvailableBotToReverseThisBtree(Btree btree) {
		for(int i = 0; i < botsCount; i++) {
			if(canThisBotReverseThisBtree(i, btree)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * This method is responsible to update the control map.
	 * @param bot
	 * @param btree
	 */
	public synchronized void updateBotBtreeReference(int bot, Btree btree) {	
		if(control.get(bot) == null) {
			HashSet<Integer> btrees = new HashSet<>();
			btrees.add(btree.getBtreeId());
			control.put(bot, btrees);
		}else {
			HashSet<Integer> btrees = control.get(bot);
			btrees.add(btree.getBtreeId());
			control.put(bot, btrees);
		}
	}
	
	/**
	 * This Method is used to run the Unit Tests
	 * @return
	 */
	public Map<Integer, HashSet<Integer>> getControl(){
		return this.control;
	}
	
	/**
	 * This Method is used to run the Unit Tests
	 * @return
	 */
	public void resetControl() {
		this.control = new HashMap<>();
	}
}

package com.botnet.domain;

/**
 * This is my representation of a Btree. 
 * It has a ID of the Btree and a ID of the node.
 * @author AUGUSTODEWESBACK
 *
 */
public class Btree{

	private int btreeId;
	private int id;
	private int value;
	private Btree leftNode;
	private Btree rightNode;

	public Btree() {
		super();
	}


	public Btree(int btreeId, int id, int value) {
		super();
		this.btreeId = btreeId;
		this.id = id;
		this.value = value;
	}

	public Btree(int btreeId, int id, int value, Btree leftNode, Btree rightNode) {
		super();
		this.btreeId = btreeId;
		this.id = id;
		this.value = value;
		this.leftNode = leftNode;
		this.rightNode = rightNode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Btree getLeftNode() {
		return leftNode;
	}

	public void setLeftNode(Btree leftNode) {
		this.leftNode = leftNode;
	}


	public Btree getRightNode() {
		return rightNode;
	}


	public void setRightNode(Btree rightNode) {
		this.rightNode = rightNode;
	}


	public int getBtreeId() {
		return btreeId;
	}


	public void setBtreeId(int btreeId) {
		this.btreeId = btreeId;
	}


}

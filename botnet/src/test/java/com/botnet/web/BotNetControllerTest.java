package com.botnet.web;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import com.botnet.configurator.BotnetApplication;
import com.botnet.domain.Btree;
import com.botnet.helper.RevertHelper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=BotnetApplication.class, webEnvironment=WebEnvironment.RANDOM_PORT)
public class BotNetControllerTest {

	
	Logger logger = Logger.getAnonymousLogger();
	
	@Autowired
	RevertHelper revertHelper;
	
	@Autowired
	BotNetController botNetController;
	
	@LocalServerPort
	int serverPort;
	
	@Value( "${bots.count}" )
	private int botsCount;
	
	@Before
	public void setServerPort() {
		revertHelper.setRemoteAddressPort(String.valueOf(serverPort));
	}
	
	@Test
	public void shouldReturnBtreeReversed() {
		Btree leftNode = new Btree(2, 1, 2);
		Btree rightNode = new Btree(2, 2, 1);
		Btree tree = new Btree(2, 0, 0, leftNode, rightNode);
		Btree returnObject = revertHelper.callRemoteRevert(tree, 0);
		assertThat(returnObject.getRightNode()).isNotNull();
		assertThat(returnObject.getLeftNode()).isNotNull();
		assertThat(returnObject.getLeftNode().getId()).isEqualTo(rightNode.getId());
		assertThat(returnObject.getRightNode().getId()).isEqualTo(leftNode.getId());
	}
	
	@Test
	public void shouldReturnExceptionBotNotFound(){
		Btree tree = new Btree(2, 0, 0, null, null);
		try {
			botNetController.bot(botsCount + 1, tree);
		}catch(ResponseStatusException e) {
			assertThat(HttpStatus.NOT_ACCEPTABLE).isEqualTo(e.getStatus());
		}
	}
}

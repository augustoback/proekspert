package com.botnet.helper;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.botnet.configurator.BotnetApplication;
import com.botnet.domain.Btree;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=BotnetApplication.class, webEnvironment=WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RevertHelperTest{
	
	@Autowired
	RevertHelper revertHelper;

	@LocalServerPort
	int serverPort;
	
	@Before
	public void setServerPort() {
		revertHelper.setRemoteAddressPort(String.valueOf(serverPort));
	}
	
	@Test
	public void shouldRevertBtreeWithoutLeftAndRightNode() {
		Btree tree = new Btree(0, 1, 0);
		Btree returnObject = revertHelper.revert(0, tree);
		assertThat(tree).isEqualTo(returnObject);
	}
	
	@Test
	public void shouldNotRevertBtreeNull() {
		Btree returnObject = revertHelper.revert(1, null);
		assertThat(returnObject).isNull();
	}
	
	@Test
	public void shouldRevertBtreeWithoutRightNode() {
		Btree leftNode = new Btree(0, 2, 1);
		Btree tree = new Btree(0, 1, 0,leftNode,null);
		Btree returnObject = revertHelper.revert(2, tree);
		assertThat(returnObject.getLeftNode()).isNull();
		assertThat(returnObject.getRightNode().getId()).isEqualTo(leftNode.getId());
	}
	
	@Test
	public void shouldRevertBtreeWithoutLeftNode() {
		Btree rightNode = new Btree(1, 2, 1);
		Btree tree = new Btree(1, 1, 0, null, rightNode);
		Btree returnObject = revertHelper.revert(3, tree);
		assertThat(returnObject.getRightNode()).isNull();
		assertThat(returnObject.getLeftNode().getId()).isEqualTo(rightNode.getId());
	}
	
	@Test
	public void shouldRevertBtreeWithTwoNodes() {
		Btree leftNode = new Btree(2, 1, 2);
		Btree rightNode = new Btree(2, 2, 1);
		Btree tree = new Btree(2, 0, 0, leftNode, rightNode);
		Btree returnObject = revertHelper.revert(0, tree);
		assertThat(returnObject.getRightNode()).isNotNull();
		assertThat(returnObject.getLeftNode()).isNotNull();
		assertThat(returnObject.getLeftNode().getId()).isEqualTo(rightNode.getId());
		assertThat(returnObject.getRightNode().getId()).isEqualTo(leftNode.getId());
	}
}

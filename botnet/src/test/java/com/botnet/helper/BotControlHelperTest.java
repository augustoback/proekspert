package com.botnet.helper;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.botnet.configurator.BotnetApplication;
import com.botnet.domain.Btree;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=BotnetApplication.class, webEnvironment=WebEnvironment.RANDOM_PORT)
public class BotControlHelperTest{

	@Autowired
	BotControlHelper botControlHelper;

	@Value( "${bots.count}" )
	private int botsCount;
	
	@Before
	public void resetControl() {
		botControlHelper.resetControl();
	}
	
	@Test()
	public void shouldReturnBotToReverseABtree() {
		assertThat(0).isEqualTo(botControlHelper.findAvailableBotToReverseThisBtree(new Btree(0, 1, 0)));
	}
	
	@Test
	public void shouldReturnNotBotToReverseABtree() {
		Btree tree = new Btree(0, 1, 0);
		for(int i = 0; i < botsCount; i++) {
			botControlHelper.updateBotBtreeReference(i, tree);
		}
		assertThat(-1).isEqualTo(botControlHelper.findAvailableBotToReverseThisBtree(tree));
	}

	@Test
	public void shouldReturnNotBotToReverse() {
		Btree tree = new Btree(0, 1, 0);
		assertThat(false).isEqualTo(botControlHelper.canThisBotReverseThisBtree(5, tree));
	}
	
	@Test
	public void shouldUpdateBotTreeReference() {
		Btree tree = new Btree(0, 1, 0);
		Btree tree2 = new Btree(2, 1, 0);
		botControlHelper.updateBotBtreeReference(5, tree);
		botControlHelper.updateBotBtreeReference(5, tree2);
		assertThat(botControlHelper.getControl().get(5)).contains(0,2);
	}
	
	@Test
	public void shouldReturnNotBotToReverseThisBtree() {
		Btree tree = new Btree(0, 1, 0);
		botControlHelper.updateBotBtreeReference(0, tree);
		assertThat(false).isEqualTo(botControlHelper.canThisBotReverseThisBtree(0, tree));
	}
}
